local nvim_lsp = require 'lspconfig'

nvim_lsp.gopls.setup {
  cmd = { 'gopls' },
  capabilities = require('lsp.handlers').capabilities,
  on_attach = require('lsp.handlers').on_attach,
  settings = {
    gopls = {
      experimentalPostfixCompletions = true,
      staticcheck = true,
      analyses = { unusedparams = true, shadow = true },
    },
  }
}
