local status_ok, _ = pcall(require, "lspconfig")
if not status_ok then
  return
end

require 'lsp.lspkind'
require "lsp.signature"
require("lsp.handlers").setup()
require "lsp.servers.go"
require 'lsp.servers.rust'
require 'lsp.lspcolors'

vim.cmd [[
  autocmd BufWritePre *.go lua vim.lsp.buf.code_action({ source = { organizeImports = true } })
  autocmd BufWritePre *.go lua vim.lsp.buf.formatting()
  autocmd BufWritePre *.rs lua vim.lsp.buf.formatting_sync(nil, 200)
]]
