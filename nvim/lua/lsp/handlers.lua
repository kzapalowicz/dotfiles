-- cmp nvim lsp is strictly required
local status_ok, cmp_nvim_lsp = pcall(require, "cmp_nvim_lsp")
if not status_ok then
  return
end

local M = {}

-- lsp_keymaps configures the language server protocol keybindings
local function lsp_keymaps(bufnr)
  local opts = { noremap = true, silent = true }
  local buf_keymap = vim.api.nvim_buf_set_keymap

  buf_keymap(bufnr, "n", "<C-]>", "<cmd>lua vim.lsp.buf.declaration()<CR>", opts)
  buf_keymap(bufnr, "n", "K", "<cmd>lua vim.lsp.buf.hover()<CR>", opts)
  buf_keymap(bufnr, "n", "gl", "<cmd>lua vim.diagnostic.open_float()<CR>", opts)
  buf_keymap(bufnr, "n", "<leader>q", "<cmd>lua vim.diagnostic.setloclist()<CR>", opts)

  buf_keymap(bufnr, "n", "gr", "<cmd>Telescope lsp_references theme=dropdown<CR>", opts)
  buf_keymap(bufnr, "n", "gi", "<cmd>Telescope lsp_implementations theme=dropdown<CR>", opts)
  buf_keymap(bufnr, "n", "gd", "<cmd>Telescope lsp_definitions theme=dropdown<CR>", opts)
  buf_keymap(bufnr, "n", "gt", "<cmd>Telescope lsp_type_definitions theme=dropdown<CR>", opts)
  buf_keymap(bufnr, "n", "gs", "<cmd>Telescope lsp_document_symbols theme=dropdown<CR>", opts)


  -- buf_keymap(bufnr, "n", "gd", "<cmd>lua vim.lsp.buf.definition()<CR>", opts)
  -- buf_keymap(bufnr, "n", "gi", "<cmd>lua vim.lsp.buf.implementation()<CR>", opts)
  -- buf_keymap(bufnr, "n", "gr", "<cmd>lua vim.lsp.buf.references()<CR>", opts)
  buf_keymap(bufnr, "n", "<C-k>", "<cmd>lua vim.lsp.buf.signature_help()<CR>", opts)
  -- buf_keymap(bufnr, "n", "<leader>rn", "<cmd>lua vim.lsp.buf.rename()<CR>", opts)
  -- buf_keymap(bufnr, "n", "<leader>ca", "<cmd>lua vim.lsp.buf.code_action()<CR>", opts)
  -- buf_keymap(bufnr, "n", "<leader>f", "<cmd>lua vim.diagnostic.open_float()<CR>", opts)
  -- buf_keymap(bufnr, "n", "[d", '<cmd>lua vim.diagnostic.goto_prev({ border = "rounded" })<CR>', opts)
  -- buf_keymap(bufnr, "n", "]d", '<cmd>lua vim.diagnostic.goto_next({ border = "rounded" })<CR>', opts)
  
  vim.cmd [[ command! LspFormat execute 'lua vim.lsp.buf.formatting()' ]]
end

-- lsp_highlight_document highlights symbols under cursor if the server supports such a functionality
local function lsp_highlight_document(client)
  -- Set autocommands conditional on server_capabilities
  if client.resolved_capabilities.document_highlight then
    vim.api.nvim_exec(
      [[
      augroup lsp_document_highlight
        autocmd! * <buffer>
        autocmd CursorHold <buffer> lua vim.lsp.buf.document_highlight()
        autocmd CursorMoved <buffer> lua vim.lsp.buf.clear_references()
      augroup END
    ]],
      false
    )
  end
end

local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities.textDocument.completion.completionItem.snippetSupport = true

-- setup functions of the exported module

M.capabilities = cmp_nvim_lsp.update_capabilities(capabilities)

M.on_attach = function(client, bufnr)
  lsp_keymaps(bufnr)
  lsp_highlight_document(client)
end

local diagfunc = function(diagnostics)
  return string.format("E: %s", diagnostic.message)
end

M.setup = function()
  -- 1. setup icons on the left pane
  local signs = {
    { name = "DiagnosticSignError", text = "x " },
    { name = "DiagnosticSignWarn", text = "> " },
    { name = "DiagnosticSignHint", text = "> " },
    { name = "DiagnosticSignInfo", text = "> " },
  }

  for _, sign in ipairs(signs) do
    vim.fn.sign_define(sign.name, { texthl = sign.name, text = sign.text, numhl = "" })
  end


  -- 2. configure the behavior
  local config = {
    virtual_text = {
      prefix = '<',
    },
    signs = { active = signs },
    update_in_insert = true,
    underline = true,
    severity_sort = true,
    source = true,
    prefix = "<",
    format = diagfunc,
    float = {
      focusable = true,
      style = "minimal",
      border = "rounded",
      source = "always",
      header = "",
      prefix = "<",
      format = diagfunc,
    },
  }

  -- this is not working atm, diagnostic is nil value 
  vim.diagnostic.config(config)

  vim.lsp.handlers["textDocument/hover"] = vim.lsp.with(vim.lsp.handlers.hover, {
    border = "rounded",
  })

  vim.lsp.handlers["textDocument/signatureHelp"] = vim.lsp.with(vim.lsp.handlers.signature_help, {
    border = "rounded",
  })
end

return M
