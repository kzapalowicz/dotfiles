local options = {
  swapfile = false,
  clipboard = "unnamedplus",
  signcolumn = "yes",
  hlsearch = true,
  number = true,
  expandtab = true,
  laststatus = 2,
  fileencoding = "utf-8",
  incsearch = true,
  ruler = true,
  history = 1000,
  termguicolors = true,
  showmode = false,
  mouse = "a",
  -- guifont="CaskaydiaCove Nerd Font Mono:h11",
  guifont = "FiraCode Nerd Font Mono:h13",
  cursorline = true,
}

vim.opt.shortmess:append "c"

for k, v in pairs(options) do
  vim.opt[k] = v
end
