local status_ok, configs = pcall(require, "nvim-treesitter.configs")
if not status_ok then
	return
end

configs.setup({
	ensure_installed = {
	  "json",
	  "yaml",
	  "html",
	  "rust",
	  "go"
	}, -- one of "all", "maintained" (parsers with maintainers), or a list of languages
	sync_install = false, -- install languages synchronously (only applied to `ensure_installed`)
	ignore_install = { "" }, -- List of parsers to ignore installing
	highlight = {
		enable = true, -- false will disable the whole extension
		disable = { "" }, -- list of language that will be disabled
		additional_vim_regex_highlighting = true,
	},
	autopairs = { enable = true },
	indent = { enable = true, disable = { "yaml", "python" } },
	context_commentstring = { enable = false, enable_autocmd = false },
	autotag = { enable = false, disable = { "xml" } },
	rainbow = {
		enable = true,
		colors = {
			"Gold",
			"Orchid",
			"DodgerBlue",
			"Salmon",
			-- "Cornsilk",
			-- "LawnGreen",
		},
		disable = { "html" },
	},
	playground = { enable = false },
})
