-- Key Mappings {{{

local opts = { noremap = true, silent = true}
local keymap = vim.api.nvim_set_keymap

keymap("", "<Space>", "<Nop>", opts)
vim.g.mapleader = " "
vim.g.maplocalleader = " "

keymap('n', '<Leader>h', ':set hlsearch!<CR>', opts)
keymap('n', '<Leader>e', ':NvimTreeToggle<CR>', opts)

-- split windows
keymap('n', 'ss', ':split<Return><C-w>w', opts)
keymap('n', 'vs', ':vsplit<Return><C-w>w', opts)

-- move between windows
keymap('n', 's<left>', '<C-w>h', opts)
keymap('n', 's<right>', '<C-w>l', opts)
keymap('n', 's<up>', '<C-w>k', opts)
keymap('n', 's<down>', '<C-w>j', opts)

keymap('n', 'sh', '<C-w>h', opts)
keymap('n', 'sj', '<C-w>j', opts)
keymap('n', 'sk', '<C-w>k', opts)
keymap('n', 'sl', '<C-w>l', opts)

-- resize windows
keymap('n', '<C-w><left>', '<C-w><', opts)
keymap('n', '<C-w><right>', '<C-w>>', opts)
keymap('n', '<C-w><up>', '<C-w>+', opts)
keymap('n', '<C-w><down>', '<C-w>-', opts)

-- telescope
keymap('n', '<C-g>', ':Telescope live_grep theme=dropdown preview=20<CR>', opts)
keymap('n', '<C-p>', ':Telescope find_files theme=dropdown previewer=false<CR>', opts)
keymap('n', ';b', ':Telescope buffers theme=dropdown preview=20<CR>', opts)

-- naviagate buffers
--keymap("n", "<S-l>", ":bnext<CR>", opts)
--keymap("n", "<S-h>", ":bprevious<CR>", opts)
-- }}}
