local fn = vim.fn
local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'
if fn.empty(fn.glob(install_path)) > 0 then
  packer_bootstrap = fn.system({'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path})
end

return require('packer').startup(function(use)
  -- Packer can manage itself
  use 'wbthomason/packer.nvim'

  use "nvim-lua/plenary.nvim"
  use 'windwp/nvim-autopairs'
  use 'kyazdani42/nvim-web-devicons'
  use {
    'kyazdani42/nvim-tree.lua',
    config = function() require'nvim-tree'.setup {} end
  }
  use "nvim-lualine/lualine.nvim"
  use "folke/zen-mode.nvim"
  use "folke/todo-comments.nvim"
  use "karb94/neoscroll.nvim"
  use { "mrjones2014/dash.nvim", run = 'make install' }
  use "onsails/lspkind-nvim"
  use 'antoinemadec/FixCursorHold.nvim'

  -- Autocompletion and Snippets
  use 'hrsh7th/nvim-cmp'
  use 'hrsh7th/cmp-buffer'
  use 'hrsh7th/cmp-path'
  use 'hrsh7th/cmp-cmdline'
  use "saadparwaiz1/cmp_luasnip"
  use "L3MON4D3/LuaSnip"
  use "rafamadriz/friendly-snippets"

  -- LSP
  use 'neovim/nvim-lspconfig'
  use "hrsh7th/cmp-nvim-lsp"
  use "ray-x/lsp_signature.nvim"
  use 'simrat39/rust-tools.nvim'
  use 'folke/lsp-colors.nvim'

  -- Colorschemes
  use { 'kaicataldo/material.vim', branch =  'main' }
  use 'overcache/NeoSolarized'
  use 'ellisonleao/gruvbox.nvim'
  use 'arcticicestudio/nord-vim'
  use 'EdenEast/nightfox.nvim'
  use 'savq/melange'
  use 'mhartington/oceanic-next'
  use 'rebelot/kanagawa.nvim'

  -- Telescope
  use "nvim-telescope/telescope.nvim"
  use "tom-anders/telescope-vim-bookmarks.nvim"
  use "nvim-telescope/telescope-media-files.nvim"
  use "nvim-telescope/telescope-ui-select.nvim"

  -- Treesitter
  use { "nvim-treesitter/nvim-treesitter", run = ":TSUpdate" }
  use "ChristianChiarulli/nvim-ts-rainbow"
  use "romgrk/nvim-treesitter-context"

  -- Git
  use "lewis6991/gitsigns.nvim"

  -- Automatically set up your configuration after cloning packer.nvim
  -- Put this at the end after all plugins
  if packer_bootstrap then
    require('packer').sync()
  end
end)
