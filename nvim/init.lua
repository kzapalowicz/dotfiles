-- Requires {{{

require('options')
require('autopairs')
require('plugins')
require('keymaps')
require('completion')
require('lsp')
require('tele-scope')
require('treesitter')
require('ts-context')
require('git-signs')
require('lua-line')
require('zen')
require('todo')
require('nscroll')
require('dashapp')

vim.cmd 'colorscheme material'
vim.cmd 'set background=dark'

-- highlight only the line number
vim.cmd [[
hi clear CursorLine
augroup CLClear
    autocmd! ColorScheme * hi clear CursorLine
augroup END

hi CursorLineNR cterm=bold
augroup CLNRSet
    autocmd! ColorScheme * hi CursorLineNR cterm=bold
augroup END
]]

-- }}}
